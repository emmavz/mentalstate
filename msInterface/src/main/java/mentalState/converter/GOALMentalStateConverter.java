/**
 * The GOAL Mental State. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package mentalState.converter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import krTools.language.DatabaseFormula;
import krTools.language.Update;
import languageTools.program.agent.Module;
import mentalState.GoalBase;
import mentalState.MentalState;
import mentalState.MentalStateWithEvents;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTQueryException;

/**
 * Represents a mental state converter. The task of instances of this class is
 * to translate a {@link MentalStateInterface} object to the binary
 * representation, and back. Each instance has a single
 * {@link MentalStateInterface} object associated with it, whose contents often
 * changes.
 * <h1>Serialization</h1>
 * <p>
 * The serialization is currently made to support loading a previously learned
 * behaviour (see {@link FileLearner}) for execution of a GOAL program. A
 * de-serialized {@link GOALMentalStateConverter} is partial: {@link Module} and
 * {@link GoalBase} are not restored fully. Also, we don't save all info stored
 * here that is relevant for learning. Therefore, a de-serialized
 * {@link GOALMentalStateConverter} can not be used to resume model checking or
 * resume learning.
 * </p>
 */
public class GOALMentalStateConverter implements Serializable {
	/** Auto-generated serial version UID */
	private static final long serialVersionUID = 6591769350769988803L;
	/**
	 * The mental state associated with this converter.
	 */
	transient private MentalState mentalState;
	/**
	 * The conversion universe that this converter has access to, which contains
	 * all beliefs and goals that have occurred in the mental state in the past.
	 */
	private GOALConversionUniverse universe;
	/**
	 * The goal bases that have occurred in the attention stack of the agent.
	 */
	transient private List<GoalBase> goalBases;

	/**
	 * Empty debugger for reuse (this object is nowhere really used, but rather
	 * instantiated to pass as parameter to methods that require so).
	 */
	// transient protected Debugger debugger = new
	// SteppingDebugger("converter",null);

	//
	// Constructors
	//

	/**
	 * Creates a converter for the specified mental state.
	 *
	 * @param mentalState
	 *            The mental state to be associated with this converter.
	 */
	public GOALMentalStateConverter(MentalState mentalState) {
		this.mentalState = mentalState;
		this.universe = new GOALConversionUniverse();
		this.goalBases = new LinkedList<>();
		// this.goalBases.add(ms.getAttentionSet());
	}

	//
	// Public methods
	//

	/**
	 * Returns the mental state associated with this converter.
	 *
	 * @return The associated mental state.
	 */
	public MentalState getMentalState() {
		return this.mentalState;
	}

	/**
	 * Returns the universe associated with this converter.
	 *
	 * @return The universe.
	 */
	public GOALConversionUniverse getUniverse() {
		return this.universe;
	}

	/**
	 * Sets the universe associated with this converter.
	 *
	 * @param The
	 *            new universe.
	 */
	public void setUniverse(GOALConversionUniverse universe) {
		this.universe = universe;
	}

	/**
	 * Translates the contents of {@link #mentalState} to a binary
	 * representation.
	 *
	 * This method is used only by model checker which also uses this class to
	 * store mental state and focus stack...
	 *
	 * @return
	 * @throws MSTQueryException
	 * @throws MSTDatabaseException
	 */
	public GOALState translate() throws MSTDatabaseException, MSTQueryException {
		Set<DatabaseFormula> beliefs = this.mentalState.getBeliefs();
		return translate(beliefs, this.mentalState.getAttentionStack());
	}

	public GOALState translate(MentalStateWithEvents mentalState, Set<String> filter)
			throws MSTDatabaseException, MSTQueryException {
		Set<DatabaseFormula> beliefs = filteredBeliefs(mentalState, filter);
		Deque<GoalBase> goalBaseStack = mentalState.getAttentionStack();
		return translate(beliefs, goalBaseStack);
	}

	/**
	 * Translates the contents of the mental state to a binary representation.
	 *
	 * @return The binary representation.
	 */
	GOALState translate(Set<DatabaseFormula> beliefs, Deque<GoalBase> goalBaseStack) {
		GOALState q = new GOALState(this);
		int index;

		// hack for #3057 to avoid ConcurrentModificationException
		List<DatabaseFormula> formulas = new ArrayList<>(beliefs);
		// Convert beliefs.
		for (DatabaseFormula formula : formulas) {
			GOALCE_Belief belief = new GOALCE_Belief(formula);
			index = this.universe.getIndex(belief);
			if (index == -1) {
				index = this.universe.addIfNotContains(belief);
			}
			q.set(index);
		}

		// Convert goals.
		int depth = 0;
		if (goalBaseStack.size() > this.goalBases.size()) {
			this.goalBases.add(goalBaseStack.peek());
		}
		for (GoalBase goalBase : goalBaseStack) {
			// Add goals per goal base on the attention stack.
			for (Update update : goalBase.getUpdates()) {
				GOALCE_GoalAtDepth goal = new GOALCE_GoalAtDepth(update, depth);
				index = this.universe.getIndex(goal);
				if (index == -1) {
					index = this.universe.addIfNotContains(goal);
				}
				q.set(index);
			}

			// Increment depth.
			depth++;
		}

		// Add foci. TODO Check.
		depth = 0;
		for (GoalBase module : goalBaseStack) {
			GOALCE_FocusAtDepth focus = new GOALCE_FocusAtDepth(module, depth);
			index = this.universe.getIndex(focus);
			if (index == -1) {
				index = this.universe.addIfNotContains(focus);
			}
			q.set(index);

			// Increment depth.
			depth++;
		}
		// Return GOALState.
		return q;
	}

	/**
	 * Returns the MentalState translated to a state vector string. The filter
	 * is applied to the MentalState before it is translated. The returned state
	 * is also added to the list of known states with a unique ID, if it is not
	 * already there.
	 *
	 * @param ms
	 * @param filter
	 * @return
	 * @throws MSTDatabaseException
	 * @throws MSTQueryException
	 */
	public String processState(MentalStateWithEvents ms, Map<String, Integer> stateid, Map<String, String> statestr,
			Set<String> filter) throws MSTDatabaseException, MSTQueryException {
		String state = "";
		this.translate(ms, filter).toString();
		if (!stateid.containsKey(state)) {
			stateid.put(state, new Integer(stateid.size() + 1));
		}

		if (!statestr.containsKey(state)) {
			String s = "";
			Set<DatabaseFormula> beliefs = filteredBeliefs(ms, filter);
			List<String> strset = new ArrayList<>(beliefs.size());
			for (DatabaseFormula dbf : beliefs) {
				strset.add(dbf.toString());
			}
			Collections.sort(strset);
			s += strset.toString() + " ";
			s += ms.printAttentionStack();
			statestr.put(state, s);
		}

		return state;
	}

	/**
	 * Returns a filtered set of beliefs whose signature matches one of those in
	 * filter.
	 *
	 * @param mentalState
	 * @param filter
	 *            A list of signatures.
	 * @return
	 * @throws MSTQueryException
	 * @throws MSTDatabaseException
	 */
	private static Set<DatabaseFormula> filteredBeliefs(MentalStateWithEvents mentalState, Set<String> filter)
			throws MSTDatabaseException, MSTQueryException {
		Set<DatabaseFormula> beliefs = new HashSet<>(mentalState.getBeliefs());
		Set<DatabaseFormula> remove = new LinkedHashSet<>();
		// hack around #3057 to prevent ConcurrentModificationException
		List<DatabaseFormula> formulas = new ArrayList<>(beliefs);
		for (DatabaseFormula belief : formulas) {
			if (!filter.contains(belief.getSignature())) {
				remove.add(belief);
			}
		}
		beliefs.removeAll(remove);
		return beliefs;
	}

	/**
	 * Updates the contents of {@link #mentalState} to correspond with the
	 * specified binary representation.
	 *
	 * @param q
	 *            The binary representation to which {@link #mentalState} should
	 *            correspond.
	 * @throws MSTDatabaseException
	 * @throws MSTQueryException
	 */
	public void update(GOALState q) throws MSTDatabaseException, MSTQueryException {
		/* Prepare number of goal bases */
		Deque<GoalBase> attentionStack = this.mentalState.getAttentionStack();
		attentionStack.clear();
		for (GoalBase goalBase : this.goalBases) {
			attentionStack.push(goalBase);
		}

		/* Get ms BitSet representation */
		List<GoalBase> focuses = new ArrayList<>(this.universe.nFocusses());
		BitSet msBitSet = translate();
		int index = -1;
		int maxIndex = Math.max(msBitSet.length(), q.length()) - 1;
		while (index++ < maxIndex) {
			/* Remove beliefs or goals */
			if (msBitSet.get(index) && !q.get(index)) {
				GOALConversionElement element = this.universe.getAtIndex(index);

				/* Belief */
				if (element instanceof GOALCE_Belief) {
					DatabaseFormula dbf = ((GOALCE_Belief) element).belief;
					this.mentalState.delete(dbf.toQuery().toUpdate());
					// debugger.breakpoint(Channel.BB_UPDATES, dbf,
					// dbf.getSourceInfo(),
					// "%s has been deleted from the belief base of %s.",
					// dbf,
					// this.mentalState.getAgentId());
					//
				}

				/* Goal */
				if (element instanceof GOALCE_GoalAtDepth) {
					// FIXME
					// GOALCE_GoalAtDepth goal = (GOALCE_GoalAtDepth) element;
					// attentionStack.get(goal.depth).remove(goal.goal);
					// debugger.breakpoint(Channel.GOAL_ACHIEVED, goal.goal,
					// goal.goal
					// .getSourceInfo(),
					// "Goal %s has been achieved and removed from the goal
					// base of %s.",
					// goal.goal.toString(), mentalState.getAgentId());
				}
			}

			/* Add beliefs or goals */
			if (!msBitSet.get(index) && q.get(index)) {
				GOALConversionElement element = this.universe.getAtIndex(index);

				/* Belief */
				if (element instanceof GOALCE_Belief) {
					this.mentalState.insert(((GOALCE_Belief) element).belief.toQuery().toUpdate());
				}

				/* Goal */
				if (element instanceof GOALCE_GoalAtDepth) {
					// FIXME
					// GOALCE_GoalAtDepth goal = (GOALCE_GoalAtDepth) element;
					// attentionStack.get(goal.depth).insert(goal.goal);
					// debugger.breakpoint(Channel.GB_UPDATES,
					// goal.goal, goal.goal.getSourceInfo(),
					// "%s has been adopted into the goal base of %s.",
					// goal.goal.toString(), mentalState.getAgentId());

				}
			}

			/* Accumulate focus names */
			if (q.get(index)) {
				GOALConversionElement element = this.universe.getAtIndex(index);
				if (element instanceof GOALCE_FocusAtDepth) {
					GOALCE_FocusAtDepth focus = (GOALCE_FocusAtDepth) element;
					focuses.add(focus.depth, focus.focus);
				}
			}
		}

		/* Remove redundant goals from attention stack */
		while (focuses.size() + 1 < attentionStack.size()) {
			attentionStack.pop();
		}

		/* Update focus names */
		for (int i = 0; i < focuses.size(); i++) {
			if (focuses.get(i) == null) {
				break;
			}
		}
	}

	/**
	 * Translates the current contents of {@link #mentalState} to a string
	 * representation.
	 *
	 * @param indent
	 *            - The margin (i.e. number of white spaces) that the string to
	 *            be constructed should adhere to.
	 * @return The string representation of {@link #mentalState}.
	 * @throws MSTDatabaseException
	 * @throws MSTQueryException
	 */
	public String toString(int indent) throws MSTDatabaseException, MSTQueryException {
		/* Process belief base */
		Set<DatabaseFormula> formulas = new LinkedHashSet<>(this.mentalState.getBeliefs().size());
		for (DatabaseFormula formula : this.mentalState.getBeliefs()) {
			formulas.add(formula);
		}

		/* Process specified indentation to whitespace */
		String tab = "";
		for (int i = 0; i < indent; i++) {
			tab += " ";
		}

		/* Build binary representation and belief base */
		String string = tab + "Bit Repres.: " + translate().toString() + "\n" + tab + "Belief base: " + formulas;

		/* Build stack of goal bases */
		int depth = 0;
		for (GoalBase goalBase : this.mentalState.getAttentionStack()) {
			String goals = "";
			for (Update goal : goalBase.getUpdates()) {
				goals += "[" + goal.toString() + "] ";
			}
			string += "\n" + tab + "Goal base " + depth + ": \"" + goalBase.getName() + "\" " + goals + "";
			depth++;
		}

		/* Return */
		return string;
	}
}
