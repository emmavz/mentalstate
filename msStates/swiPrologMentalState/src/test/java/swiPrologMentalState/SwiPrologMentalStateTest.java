
package swiPrologMentalState;

import eis.iilang.Percept;
import krTools.KRInterface;
import krTools.language.DatabaseFormula;
import krTools.language.Query;
import krTools.language.Term;
import krTools.language.Update;
import languageTools.program.agent.AgentId;
import languageTools.program.mas.AgentDefinition;
import mentalState.MentalState;
import mentalState.MentalStateTest;
import swiPrologMentalState.translator.SwiPrologTranslator;
import swiprolog.SwiPrologInterface;
import swiprolog.language.PrologCompound;
import swiprolog.language.PrologQuery;
import swiprolog.language.impl.PrologAtomImpl;
import swiprolog.language.impl.PrologCompoundImpl;
import swiprolog.language.impl.PrologDBFormulaImpl;
import swiprolog.language.impl.PrologIntImpl;
import swiprolog.language.impl.PrologQueryImpl;
import swiprolog.language.impl.PrologUpdateImpl;

public class SwiPrologMentalStateTest extends MentalStateTest {
	@Override
	protected KRInterface getKRI() throws Exception {
		return new SwiPrologInterface();
	}

	@Override
	protected MentalState getMentalState(AgentDefinition owner, AgentId agentId, boolean addAgentModel)
			throws Exception {
		return new SwiPrologMentalState(owner, agentId, addAgentModel);
	}

	@Override
	protected DatabaseFormula getDBFormula(char content) throws Exception {
		return new PrologDBFormulaImpl(new PrologAtomImpl(String.valueOf(content), null));
	}

	@Override
	protected Update getUpdate(char content) throws Exception {
		return new PrologUpdateImpl(new PrologAtomImpl(String.valueOf(content), null));
	}

	@Override
	protected Query getQuery(char content, int arg) throws Exception {
		return new PrologQueryImpl(
				new PrologCompoundImpl(String.valueOf(content), new Term[] { new PrologIntImpl(arg, null) }, null));
	}

	@Override
	protected Percept getPercept(Query query) throws Exception {
		PrologCompound compound = ((PrologQuery) query).getCompound();
		return new Percept(compound.getName(), new SwiPrologTranslator().convert(compound.getArg(0)));
	}

	@Override
	protected DatabaseFormula getMessage(char sender, char content) throws Exception {
		Term senderTerm = new PrologAtomImpl(String.valueOf(sender), null);
		Term contentTerm = new PrologAtomImpl(String.valueOf(content), null);
		return new PrologDBFormulaImpl(
				new PrologCompoundImpl("received", new Term[] { senderTerm, contentTerm }, null));
	}
}
