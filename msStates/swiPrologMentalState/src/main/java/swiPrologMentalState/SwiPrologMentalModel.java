package swiPrologMentalState;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import krTools.language.DatabaseFormula;
import krTools.language.Query;
import krTools.language.Substitution;
import krTools.language.Term;
import languageTools.program.agent.AgentId;
import languageTools.program.agent.msg.SentenceMood;
import mentalState.BASETYPE;
import mentalState.GoalBase;
import mentalState.MentalBase;
import mentalState.MentalModel;
import mentalState.MentalState;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTQueryException;
import mentalState.error.MSTTranslationException;
import mentalState.translator.Translator;
import swiPrologMentalState.translator.SwiPrologTranslator;
import swiprolog.language.PrologCompound;
import swiprolog.language.PrologQuery;
import swiprolog.language.impl.PrologCompoundImpl;
import swiprolog.language.impl.PrologQueryImpl;
import swiprolog.language.impl.PrologVarImpl;
import swiprolog.parser.PrologOperators;

public class SwiPrologMentalModel extends MentalModel {
	SwiPrologMentalModel(MentalState owner) {
		super(owner);
	}

	@Override
	protected void initialize(AgentId agent) throws MSTDatabaseException, MSTQueryException {
		// Check if the agent model we want to add is for the agent that owns
		// this database. In that case, bases that we can never use selectors on
		// are created. The other bases are always created.
		boolean me = this.owner.getAgentId().equals(agent);
		if (me) {
			// Create a knowledge base and add the agent's knowledge to it
			MentalBase knowledge = new SwiPrologMentalBase(this.owner, agent, BASETYPE.KNOWLEDGEBASE);
			List<DatabaseFormula> dbfs = this.owner.getOwner().getAllKnowledge();
			for (DatabaseFormula dbf : dbfs) {
				knowledge.insert(dbf);
			}
			addBase(knowledge, BASETYPE.KNOWLEDGEBASE);
			// Create a message base (only for ourselves)
			SwiPrologMentalBase messages = new SwiPrologMentalBase(this.owner, agent, BASETYPE.MESSAGEBASE);
			addBase(messages, BASETYPE.MESSAGEBASE);
		}

		// Create the belief, percept, and goal bases for which selectors are
		// applicable (i.e. apply to any agent we want to add a model for).
		// Note that the order here is important...
		SwiPrologMentalBase percepts = new SwiPrologMentalBase(this.owner, agent, BASETYPE.PERCEPTBASE);
		addBase(percepts, BASETYPE.PERCEPTBASE);
		SwiPrologGoalBase goals = (SwiPrologGoalBase) createGoalBase(me ? "main" : agent.getName());
		addGoalBase(goals);
		SwiPrologMentalBase beliefs = new SwiPrologMentalBase(this.owner, agent, BASETYPE.BELIEFBASE);
		addBase(beliefs, BASETYPE.BELIEFBASE);
	}

	@Override
	public GoalBase createGoalBase(String name) throws MSTDatabaseException {
		return new SwiPrologGoalBase(this.owner, name);
	}

	@Override
	protected Set<Substitution> beliefQuery(Query query) throws MSTQueryException {
		return getBase(BASETYPE.BELIEFBASE).query(query);
	}

	@Override
	protected Set<Substitution> perceptQuery(Query query) throws MSTQueryException {
		Query percept = processPercept((PrologQuery) query);
		return getBase(BASETYPE.PERCEPTBASE).query(percept);
	}

	@Override
	protected Set<Substitution> messageQuery(Query query, SentenceMood mood, List<AgentId> senders)
			throws MSTQueryException {
		if (senders.isEmpty()) {
			Term from = new PrologVarImpl("_", null);
			Query message = processMessage((PrologQuery) query, mood, from);
			return getBase(BASETYPE.MESSAGEBASE).query(message);
		} else {
			Translator translator = new SwiPrologTranslator();
			Set<Substitution> returned = new LinkedHashSet<>();
			for (AgentId sender : senders) {
				try {
					Term from = translator.convert(sender);
					Query message = processMessage((PrologQuery) query, mood, from);
					returned.addAll(getBase(BASETYPE.MESSAGEBASE).query(message));
				} catch (MSTTranslationException e) {
					throw new MSTQueryException("unable to translate message sender '" + sender + "' to Prolog.", e);
				}
			}
			return returned;
		}
	}

	@Override
	protected Set<Substitution> messageQuery(Query query, SentenceMood mood, Term var) throws MSTQueryException {
		Query message = processMessage((PrologQuery) query, mood, var);
		return getBase(BASETYPE.MESSAGEBASE).query(message);
	}

	// Helper functions
	// TODO: partial duplication with functions in SwiprologMentalState
	private Query processPercept(PrologQuery percept) throws MSTQueryException {
		List<Term> conjuncts = percept.getCompound().getOperands(",");
		List<Term> percepts = new ArrayList<>(conjuncts.size());
		for (Term conjunct : conjuncts) {
			if (isBuiltIn(conjunct)) {
				percepts.add(conjunct);
			} else {
				Term compound = new PrologCompoundImpl("percept", new Term[] { conjunct }, percept.getSourceInfo());
				percepts.add(compound);
			}
		}
		return new PrologQueryImpl(SwiPrologTranslator.termsToConjunct(percepts, percept.getSourceInfo()));
	}

	private Query processMessage(PrologQuery message, SentenceMood mood, Term sender) throws MSTQueryException {
		List<Term> conjuncts = message.getCompound().getOperands(",");
		List<Term> messages = new ArrayList<>(conjuncts.size());
		for (Term conjunct : conjuncts) {
			if (isBuiltIn(conjunct)) {
				messages.add(conjunct);
			} else {
				Term content;
				switch (mood) {
				case IMPERATIVE:
					content = new PrologCompoundImpl("imp", conjuncts.toArray(new Term[conjuncts.size()]),
							message.getSourceInfo());
					break;
				case INTERROGATIVE:
					content = new PrologCompoundImpl("int", conjuncts.toArray(new Term[conjuncts.size()]),
							message.getSourceInfo());
					break;
				default:
					content = conjunct;
					break;
				}
				PrologCompound compound = new PrologCompoundImpl("received", new Term[] { sender, content },
						message.getSourceInfo());
				messages.add(compound);
			}
		}
		return new PrologQueryImpl(SwiPrologTranslator.termsToConjunct(messages, message.getSourceInfo()));
	}

	private static boolean isBuiltIn(Term conjunct) {
		String sig = conjunct.getSignature();
		if (PrologOperators.prologBuiltin(sig)) {
			if (sig.equals("not/1")) {
				Term content = ((PrologCompound) conjunct).getArg(0);
				return PrologOperators.prologBuiltin(content.getSignature());
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}
