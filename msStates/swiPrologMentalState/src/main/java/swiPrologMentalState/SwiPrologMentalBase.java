package swiPrologMentalState;

import java.util.Set;

import krTools.exceptions.KRDatabaseException;
import krTools.exceptions.KRQueryFailedException;
import krTools.language.DatabaseFormula;
import krTools.language.Query;
import krTools.language.Substitution;
import languageTools.program.agent.AgentId;
import mentalState.BASETYPE;
import mentalState.MentalBase;
import mentalState.MentalState;
import mentalState.Result;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTQueryException;
import swiprolog.database.PrologDatabase;

public class SwiPrologMentalBase extends MentalBase {
	/**
	 * The database used for storing the contents of this base.
	 */
	private final PrologDatabase database;

	protected SwiPrologMentalBase(MentalState owner, AgentId forAgent, BASETYPE type) throws MSTDatabaseException {
		super(owner, forAgent, type);
		try {
			this.database = (PrologDatabase) owner.getOwner().getKRInterface()
					.getDatabase(owner.getAgentId() + ":" + forAgent + ":" + type, null);
			this.owner.createdDatabase(this.database, type);
		} catch (KRDatabaseException e) {
			throw new MSTDatabaseException("could not create a database for '" + owner.getOwner() + "'.", e);
		}
	}

	PrologDatabase getDatabase() {
		return this.database;
	}

	@Override
	public void destroy() throws MSTDatabaseException {
		try {
			this.database.destroy();
		} catch (KRDatabaseException e) {
			throw new MSTDatabaseException("could not destroy '" + this.database.getName() + "'.", e);
		}
	}

	@Override
	public Set<Substitution> query(Query formula) throws MSTQueryException {
		try {
			return this.database.query(formula);
		} catch (KRQueryFailedException e) {
			throw new MSTQueryException(
					String.format("failed to query '%s' on '%s'.", formula.toString(), this.database.getName()), e);
		}
	}

	@Override
	public Result insert(DatabaseFormula formula) throws MSTQueryException {
		try {
			Result result = this.owner.createResult(this.type, this.forAgent.getName());
			if (this.database.insert(formula)) {
				result.added(formula);
			}
			return result;
		} catch (KRDatabaseException e) {
			throw new MSTQueryException(
					String.format("failed to add '%s' to '%s'.", formula.toString(), this.database.getName()), e);
		}
	}

	@Override
	public Result delete(DatabaseFormula formula) throws MSTQueryException {
		try {
			Result result = this.owner.createResult(this.type, this.forAgent.getName());
			if (this.database.delete(formula)) {
				result.removed(formula);
			}
			return result;
		} catch (KRDatabaseException e) {
			throw new MSTQueryException(
					String.format("failed to delete '%s' from '%s'.", formula.toString(), this.database.getName()), e);
		}
	}
}
