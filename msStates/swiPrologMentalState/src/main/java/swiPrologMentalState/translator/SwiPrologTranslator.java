/**
 * The GOAL Mental State. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package swiPrologMentalState.translator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.jpl7.JPL;

import eis.iilang.Action;
import eis.iilang.Function;
import eis.iilang.Identifier;
import eis.iilang.Numeral;
import eis.iilang.Parameter;
import eis.iilang.ParameterList;
import eis.iilang.Percept;
import eis.iilang.TruthValue;
import krTools.language.DatabaseFormula;
import krTools.language.Term;
import krTools.language.Update;
import krTools.parser.SourceInfo;
import languageTools.program.actionspec.UserSpecAction;
import languageTools.program.agent.AgentId;
import languageTools.program.agent.msg.Message;
import languageTools.program.agent.msg.SentenceMood;
import mentalState.error.MSTTranslationException;
import mentalState.translator.Translator;
import swiprolog.language.PrologCompound;
import swiprolog.language.PrologDBFormula;
import swiprolog.language.PrologQuery;
import swiprolog.language.PrologVar;
import swiprolog.language.impl.PrologAtomImpl;
import swiprolog.language.impl.PrologCompoundImpl;
import swiprolog.language.impl.PrologDBFormulaImpl;
import swiprolog.language.impl.PrologFloatImpl;
import swiprolog.language.impl.PrologIntImpl;
import swiprolog.language.impl.PrologUpdateImpl;

public class SwiPrologTranslator implements Translator {
	@Override
	public Term convert(Parameter parameter) throws MSTTranslationException {
		if (parameter instanceof Identifier) {
			// do not do quoting of the term, that is only for printing.
			return new PrologAtomImpl(((Identifier) parameter).getValue(), null);
		} else if (parameter instanceof Numeral) {
			// check if parameter that is passed is a float.
			// note that LONG numbers are converted to float
			Number number = ((Numeral) parameter).getValue();
			if (number instanceof Double || number instanceof Float || number.longValue() < Integer.MIN_VALUE
					|| number.longValue() > Integer.MAX_VALUE) {
				return new PrologFloatImpl(number.doubleValue(), null);
			} else {
				return new PrologIntImpl(number.longValue(), null);
			}
		} else if (parameter instanceof Function) {
			Function f = (Function) parameter;
			List<Term> terms = new ArrayList<>(f.getParameters().size());
			for (Parameter p : f.getParameters()) {
				terms.add(convert(p));
			}
			return new PrologCompoundImpl(f.getName(), terms.toArray(new Term[terms.size()]), null);
		} else if (parameter instanceof ParameterList) {
			ParameterList pl = (ParameterList) parameter;
			List<Term> terms = new ArrayList<>(pl.size());
			for (Parameter p : pl) {
				terms.add(convert(p));
			}
			return makeList(terms);
		} else if (parameter instanceof TruthValue) {
			return new PrologAtomImpl(((TruthValue) parameter).getValue(), null);
		} else {
			throw new MSTTranslationException("encountered EIS parameter '" + parameter + "' of unsupported type '"
					+ parameter.getClass().getCanonicalName() + "'.");
		}
	}

	@Override
	public Parameter convert(Term term) throws MSTTranslationException {
		if (term instanceof PrologIntImpl) {
			return new Numeral(((PrologIntImpl) term).intValue());
		} else if (term instanceof PrologFloatImpl) {
			return new Numeral(((PrologFloatImpl) term).floatValue());
		} else if (term instanceof PrologAtomImpl) {
			return new Identifier(((PrologAtomImpl) term).getName());
		} else if (term instanceof PrologVar) {
			throw new MSTTranslationException("conversion of the variable '" + term
					+ "' to an EIS parameter is not possible: EIS does not support variables.");
		} else if (term instanceof PrologCompound) {
			PrologCompound compound = (PrologCompound) term;
			LinkedList<Parameter> parameters = new LinkedList<>();
			// Check whether we're dealing with a list or other operator.
			if (compound.getName().equals(JPL.LIST_PAIR)) {
				for (Term arg : compound.getOperands(JPL.LIST_PAIR)) {
					parameters.add(convert(arg));
				}
				// Remove the empty list.
				parameters.removeLast();
				return new ParameterList(parameters);
			} else {
				for (Term arg : compound) {
					parameters.add(convert(arg));
				}
				return new Function(compound.getName(), parameters);
			}
		} else {
			throw new MSTTranslationException("conversion of the term '" + term + "' of type '"
					+ term.getClass().getCanonicalName() + "' to an EIS parameter is not supported.");
		}
	}

	@Override
	public Action convert(UserSpecAction action) throws MSTTranslationException {
		LinkedList<Parameter> parameters = new LinkedList<>();
		for (Term term : action.getParameters()) {
			parameters.add(convert(term));
		}
		return new Action(action.getName(), parameters);
	}

	@Override
	public DatabaseFormula convertPercept(Percept percept) throws MSTTranslationException {
		// Get main operator name and parameters of the percept.
		String name = percept.getName();
		List<Parameter> parameters = percept.getParameters();
		// Construct a compound from the percept operator and parameters.
		List<Term> terms = new ArrayList<>(parameters.size());
		for (Parameter parameter : parameters) {
			terms.add(convert(parameter));
		}
		PrologCompound compound1 = new PrologCompoundImpl(name, terms.toArray(new Term[terms.size()]), null);
		PrologCompound compound2 = new PrologCompoundImpl("percept", new Term[] { compound1 }, null);
		return new PrologDBFormulaImpl(compound2);
	}

	@Override
	public Percept convertPercept(DatabaseFormula dbf) throws MSTTranslationException {
		PrologCompound content = ((PrologDBFormula) dbf).getCompound();
		PrologCompound percept = (PrologCompound) content.getArg(0);
		LinkedList<Parameter> parameters = new LinkedList<>();
		for (Term parameter : percept) {
			parameters.add(convert(parameter));
		}
		return new Percept(percept.getName(), parameters);
	}

	@Override
	public DatabaseFormula convertMessage(Message message) throws MSTTranslationException {
		PrologCompound content = ((PrologQuery) message.getContent().toQuery()).getCompound();
		switch (message.getMood()) {
		case IMPERATIVE:
			content = new PrologCompoundImpl("imp", new Term[] { content }, content.getSourceInfo());
			break;
		case INTERROGATIVE:
			content = new PrologCompoundImpl("int", new Term[] { content }, content.getSourceInfo());
			break;
		default:
		}
		Term sender = convert(message.getSender());
		PrologCompound compound = new PrologCompoundImpl("received", new Term[] { sender, content },
				content.getSourceInfo());
		return new PrologDBFormulaImpl(compound);
	}

	@Override
	public Message convertMessage(DatabaseFormula formula) throws MSTTranslationException {
		PrologCompound message = ((PrologDBFormula) formula).getCompound();
		PrologCompound sender = (PrologCompound) message.getArg(0);
		PrologCompound content = (PrologCompound) message.getArg(1);

		SentenceMood mood = SentenceMood.INDICATIVE;
		if (content.getArity() == 1) {
			switch (content.getName()) {
			case "imp":
				mood = SentenceMood.IMPERATIVE;
				content = (PrologCompound) content.getArg(0);
				break;
			case "int":
				mood = SentenceMood.INDICATIVE;
				content = (PrologCompound) content.getArg(0);
				break;
			default:
				break;
			}
		}

		Message returned = new Message(new PrologUpdateImpl(content), mood);
		returned.setSender(new AgentId(sender.getName()));
		return returned;
	}

	@Override
	public Term convert(AgentId id) throws MSTTranslationException {
		// TODO: can we use this method in general?
		return convert(new Identifier(id.getName()));
	}

	@Override
	public Term makeList(List<Term> terms) throws MSTTranslationException {
		SourceInfo source = null;
		if (!terms.isEmpty()) {
			source = terms.get(0).getSourceInfo();
		}
		// Start with element in list, since innermost term of Prolog list is
		// the last term.
		Term list = new PrologAtomImpl(JPL.LIST_NIL.name(), source);
		for (int i = terms.size() - 1; i >= 0; i--) {
			list = new PrologCompoundImpl(JPL.LIST_PAIR, new Term[] { terms.get(i), list }, source);
		}
		return list;
	}

	@Override
	public Update makeUpdate(List<DatabaseFormula> formulas) throws MSTTranslationException {
		List<Term> terms = new ArrayList<>(formulas.size());
		for (DatabaseFormula add : formulas) {
			terms.add(((PrologDBFormula) add).getCompound());
		}
		return new PrologUpdateImpl(termsToConjunct(terms, null));
	}

	/**
	 * Returns a (possibly empty) conjunct containing the given terms.
	 *
	 * @param terms
	 * @param info
	 *            source info
	 * @return possibly empty conjunct containing the given terms
	 */
	public static PrologCompound termsToConjunct(List<Term> terms, SourceInfo info) {
		if (terms.isEmpty()) {
			return new PrologAtomImpl("true", info);
		} else {
			// build up list last to first.
			PrologCompound list = (PrologCompound) terms.get(terms.size() - 1); // last
			for (int i = terms.size() - 2; i >= 0; i--) {
				list = new PrologCompoundImpl(",", new Term[] { terms.get(i), list }, info);
			}
			return list;
		}
	}
}
