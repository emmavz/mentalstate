/**

 * The GOAL Mental State. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package swiPrologMentalState;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import eis.iilang.Percept;
import krTools.database.Database;
import krTools.exceptions.KRDatabaseException;
import krTools.exceptions.KRQueryFailedException;
import krTools.language.DatabaseFormula;
import krTools.language.Term;
import krTools.language.Update;
import languageTools.program.agent.AgentId;
import languageTools.program.agent.msg.Message;
import languageTools.program.mas.AgentDefinition;
import mentalState.BASETYPE;
import mentalState.GoalBase;
import mentalState.MentalBase;
import mentalState.MentalModel;
import mentalState.MentalState;
import mentalState.Result;
import mentalState.SingleGoal;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTQueryException;
import mentalState.error.MSTTranslationException;
import mentalState.translator.Translator;
import swiPrologMentalState.translator.SwiPrologTranslator;
import swiprolog.database.PrologDatabase;
import swiprolog.language.PrologCompound;
import swiprolog.language.PrologDBFormula;
import swiprolog.language.PrologQuery;
import swiprolog.language.PrologTerm;
import swiprolog.language.impl.PrologAtomImpl;
import swiprolog.language.impl.PrologCompoundImpl;
import swiprolog.language.impl.PrologIntImpl;
import swiprolog.language.impl.PrologQueryImpl;

/**
 * The knowledge representation (KR) interface with GOAL specific extra
 * functionality.
 */
public class SwiPrologMentalState extends MentalState {
	protected Translator translator = new SwiPrologTranslator();

	/**
	 * @see {@link MentalState#MentalState(AgentDefinition, AgentId, AgentRegistry)}
	 */
	public SwiPrologMentalState(AgentDefinition owner, AgentId agentId) throws MSTDatabaseException, MSTQueryException {
		super(owner, agentId);
	}

	/**
	 * @see {@link MentalState#MentalState(AgentDefinition, AgentId, AgentRegistry, boolean)}
	 */
	protected SwiPrologMentalState(AgentDefinition owner, AgentId agentId, boolean addAgentModel)
			throws MSTDatabaseException, MSTQueryException {
		super(owner, agentId, addAgentModel);
	}

	@Override
	public Result createResult(BASETYPE base, String focus) {
		return new SwiPrologResult(base, focus);
	}

	@Override
	protected MentalModel createMentalModel() {
		return new SwiPrologMentalModel(this);
	}

	@Override
	public void createdDatabase(Database database, BASETYPE type) throws MSTDatabaseException {
		PrologDatabase db = (PrologDatabase) database;
		switch (type) {
		case BELIEFBASE:
		case GOALBASE:
			// In the belief and goal bases, all knowledge is added.
			try {
				db.addKnowledge(getKnowledge());
				break;
			} catch (KRDatabaseException | MSTQueryException e) {
				throw new MSTDatabaseException("unable to impose knowledge on SWI database '" + db.getName() + "'.", e);
			}
		case PERCEPTBASE:
			// In the perceptbase we declare percept/1,
			// which is actually a bit of a hack to support
			// storing and querying any percept (i.e. wrapping it inside a
			// percept(Percept) predicate every time).
			// No knowledge is added here.
			try {
				PrologTerm percept = new PrologAtomImpl("percept", null);
				PrologTerm one = new PrologIntImpl(1, null);
				PrologCompound signature = new PrologCompoundImpl("/", new Term[] { percept, one }, null);
				PrologCompound dynamic = new PrologCompoundImpl("dynamic", new Term[] { signature }, null);
				db.query(new PrologQueryImpl(dynamic));
				break;
			} catch (KRQueryFailedException e) {
				throw new MSTDatabaseException("unable to declare percept/1 in SWI database '" + db.getName() + "'.",
						e);
			}
		case MESSAGEBASE:
			// In the messagebase we declare received/2,
			// which is actually a bit of a hack to support
			// storing and querying any message along with its sender (i.e.
			// wrapping it inside a received(Sender,Msg) predicate every time).
			// No knowledge is added here.
			try {
				PrologTerm received = new PrologAtomImpl("received", null);
				PrologTerm two = new PrologIntImpl(2, null);
				PrologCompound signature = new PrologCompoundImpl("/", new Term[] { received, two }, null);
				PrologCompound dynamic = new PrologCompoundImpl("dynamic", new Term[] { signature }, null);
				db.query(new PrologQueryImpl(dynamic));
				break;
			} catch (KRQueryFailedException e) {
				throw new MSTDatabaseException("unable to declare received/2 in SWI database '" + db.getName() + "'.",
						e);
			}
		default:
			break;
		}
		// TODO (sometime): how to know who I am (and who the other agents are)?
	}

	@Override
	public void declare(BASETYPE basetype, List<DatabaseFormula> formulas)
			throws MSTDatabaseException, MSTQueryException {
		for (DatabaseFormula formula : formulas) {
			PrologCompound compound = ((PrologDBFormula) formula).getCompound();
			PrologCompound dynamic = new PrologCompoundImpl("dynamic", new Term[] { compound },
					compound.getSourceInfo());
			PrologQuery declare = new PrologQueryImpl(dynamic);
			if (basetype == BASETYPE.GOALBASE) {
				for (GoalBase base : getAttentionStack()) {
					for (SingleGoal goal : base) {
						goal.query(declare);
					}
				}
			} else {
				for (AgentId agent : getKnownAgents()) {
					getModel(agent).getBase(basetype).query(declare);
				}
			}
		}
	}

	@Override
	public List<Result> insert(Update update, AgentId... agent) throws MSTDatabaseException, MSTQueryException {
		if (agent.length == 0) {
			agent = new AgentId[] { this.agentId };
		}
		List<Result> results = new LinkedList<>();
		for (AgentId id : agent) {
			MentalBase base = getModel(id).getBase(BASETYPE.BELIEFBASE);
			results.add(base.insert(update));
		}
		return results;
	}

	@Override
	public Result received(Message message) throws MSTDatabaseException, MSTQueryException {
		try {
			DatabaseFormula formula = this.translator.convertMessage(message);
			// TODO: in the future we would want to support mental models here
			return getOwnModel().getBase(BASETYPE.MESSAGEBASE).insert(formula);
		} catch (MSTTranslationException e) {
			throw new MSTQueryException("unable to process message '" + message + "'.", e);
		}
	}

	@Override
	public List<Result> delete(Update update, AgentId... agent) throws MSTDatabaseException, MSTQueryException {
		if (agent.length == 0) {
			agent = new AgentId[] { this.agentId };
		}
		List<Result> results = new LinkedList<>();
		for (AgentId id : agent) {
			MentalBase base = getModel(id).getBase(BASETYPE.BELIEFBASE);
			results.add(base.delete(update));
		}
		return results;
	}

	@Override
	public Result removeMessage(Message message) throws MSTDatabaseException, MSTQueryException {
		try {
			DatabaseFormula formula = this.translator.convertMessage(message);
			// TODO: in the future we would want to support mental models here
			return getOwnModel().getBase(BASETYPE.MESSAGEBASE).delete(formula);
		} catch (MSTTranslationException e) {
			throw new MSTQueryException("unable to process message '" + message + "'.", e);
		}
	}

	@Override
	public List<Result> percept(Percept percept, AgentId... agent) throws MSTDatabaseException, MSTQueryException {
		if (agent.length == 0) {
			agent = new AgentId[] { this.agentId };
		}
		List<Result> results = new LinkedList<>();
		for (AgentId id : agent) {
			try {
				MentalBase base = getModel(id).getBase(BASETYPE.PERCEPTBASE);
				DatabaseFormula formula = this.translator.convertPercept(percept);
				results.add(base.insert(formula));
			} catch (MSTTranslationException e) {
				throw new MSTQueryException("unable to process percept '" + percept + "'.", e);
			}
		}
		return results;
	}

	@Override
	public List<Result> removePercept(Percept percept, AgentId... agent)
			throws MSTDatabaseException, MSTQueryException {
		if (agent.length == 0) {
			agent = new AgentId[] { this.agentId };
		}
		List<Result> results = new LinkedList<>();
		for (AgentId id : agent) {
			try {
				MentalBase base = getModel(id).getBase(BASETYPE.PERCEPTBASE);
				DatabaseFormula formula = this.translator.convertPercept(percept);
				results.add(base.delete(formula));
			} catch (MSTTranslationException e) {
				throw new MSTQueryException("unable to process percept percept '" + percept + "'.", e);
			}
		}
		return results;
	}

	@Override
	public Set<DatabaseFormula> getKnowledge() throws MSTDatabaseException, MSTQueryException {
		SwiPrologMentalBase base = (SwiPrologMentalBase) getOwnModel().getBase(BASETYPE.KNOWLEDGEBASE);
		return base.getDatabase().getTheory().getFormulas();
	}

	@Override
	public Set<DatabaseFormula> getBeliefs(AgentId... agent) throws MSTDatabaseException, MSTQueryException {
		AgentId id = (agent.length == 0) ? this.agentId : agent[0];
		SwiPrologMentalBase base = (SwiPrologMentalBase) getModel(id).getBase(BASETYPE.BELIEFBASE);
		return base.getDatabase().getTheory().getFormulas();
	}

	@Override
	public int getBeliefCount() {
		int count = 0;
		for (MentalModel model : getActiveModels()) {
			SwiPrologMentalBase base = (SwiPrologMentalBase) model.getBase(BASETYPE.BELIEFBASE);
			count += base.getDatabase().getTheory().getFormulas().size();
		}
		return count;
	}

	@Override
	public Set<Percept> getPercepts(AgentId... agent) throws MSTDatabaseException, MSTQueryException {
		AgentId id = (agent.length == 0) ? this.agentId : agent[0];
		SwiPrologMentalBase base = (SwiPrologMentalBase) getModel(id).getBase(BASETYPE.PERCEPTBASE);
		Set<Percept> returned = new LinkedHashSet<>();
		for (DatabaseFormula formula : base.getDatabase().getTheory().getFormulas()) {
			try {
				Percept percept = this.translator.convertPercept(formula);
				returned.add(percept);
			} catch (MSTTranslationException e) {
				throw new MSTQueryException("unable to process percept formula '" + formula + "'.", e);
			}
		}
		return returned;
	}

	@Override
	public int getPerceptCount() {
		int count = 0;
		for (MentalModel model : getActiveModels()) {
			SwiPrologMentalBase base = (SwiPrologMentalBase) model.getBase(BASETYPE.PERCEPTBASE);
			count += base.getDatabase().getTheory().getFormulas().size();
		}
		return count;
	}

	@Override
	public Set<Message> getMessages() throws MSTDatabaseException, MSTQueryException {
		SwiPrologMentalBase base = (SwiPrologMentalBase) getOwnModel().getBase(BASETYPE.MESSAGEBASE);
		Set<Message> returned = new LinkedHashSet<>();
		for (DatabaseFormula formula : base.getDatabase().getTheory().getFormulas()) {
			try {
				Message message = this.translator.convertMessage(formula);
				returned.add(message);
			} catch (MSTTranslationException e) {
				throw new MSTQueryException("unable to process message formula '" + formula + "'.", e);
			}
		}
		return returned;
	}

	@Override
	public int getMessageCount() {
		SwiPrologMentalBase base = (SwiPrologMentalBase) getOwnModel().getBase(BASETYPE.MESSAGEBASE);
		return base.getDatabase().getTheory().getFormulas().size();
	}
}
